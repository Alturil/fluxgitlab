import requests
import pytest

subcategories = requests.get("https://api.tmsandbox.co.nz/v1/Categories/UsedCars.json?with_counts=true").json()["Subcategories"]

# Return how many named brands of used cars are available in the TradeMe UsedCars category.
def test_amount_of_named_brands_is_greater_than_zero():
    namedBrands = [o["Name"] for o in subcategories]
    assert len(namedBrands) > 0

# "Check that the brand ‘Kia’ exists."
def test_kia_brand_exists():
    namedBrands = [o["Name"] for o in subcategories]
    assert "Kia" in namedBrands

# Return the current number of Kia cars listed.
def test_amount_of_kia_cars_greater_than_zero():
    kia = next((k for k in subcategories if k["Name"] == "Kia"), None)
    assert kia["Count"] > 0

# Check that the brand ‘Hispano Suiza’ does not exist.
def test_hispano_suiza_brand_does_not_exist():
    namedBrands = [o["Name"] for o in subcategories]    
    assert "Hispano Suiza" not in namedBrands