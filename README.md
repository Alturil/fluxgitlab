# Flux - GitLab project

This repo complements the exercise in [GitHub](https://github.com/Alturil/flux) by adding the option of runnig a few versions of the same tests in parallel as different jobs in a CI pipeline:

- Javascript tests
- Postman tests
- Python tests

The idea is that any further commit to the test repo will trigger this pipeline (see `.gitlab-ci.yml`), and therefore the tests run remotely, removing any dependencies on the user.

## How to run the tests

Tests can be run from the [Pipelines](https://gitlab.com/Alturil/fluxgitlab/pipelines) section. Only contributors of the project are allowed to do so, though, so you'll have to send me your GitLab user so that I can approve you as a contributor.

Alternatively, an example run can be found [HERE](https://gitlab.com/Alturil/fluxgitlab/pipelines/135132837), and the links to their respective reports below:

- [Javascript tests](https://alturil.gitlab.io/-/fluxgitlab/-/jobs/507835034/artifacts/.artifacts/javascript_report.html)
- [Postman tests](https://alturil.gitlab.io/-/fluxgitlab/-/jobs/507835035/artifacts/.artifacts/postman_report.html)
- [Python tests](https://alturil.gitlab.io/-/fluxgitlab/-/jobs/507835036/artifacts/.artifacts/python_report.html)

## Versions

### Javascript tests

- The tests are run via JS when loading the `javascript_results.html` page
- The results are rendered in the page, which serves as a script report

### Postman tests

- The tests themselves are in a json file
- The environment variables are in a separate json file (it only contains the Sandbox url at the moment)
- The test results are saved to `postman_results.html`, and use the [HtmlExtra](https://github.com/DannyDainton/newman-reporter-htmlextra) report.

### Python tests

- The tests are defined in the `checks.py` file
- I'm using [pytest](https://docs.pytest.org/en/latest/) as a framework
- The results are saved to `python_results.html`, and use the [pytest-html](https://pypi.org/project/pytest-html/) report